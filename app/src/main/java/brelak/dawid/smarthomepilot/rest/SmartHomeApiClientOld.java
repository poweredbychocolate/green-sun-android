package brelak.dawid.smarthomepilot.rest;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;
import brelak.dawid.smarthomepilot.model.device.basic.DeviceResource;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SmartHomeApiClientOld {
    public Retrofit getApiClient(String severAddress){

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(DeviceResource.class, new Adapter());
        Gson gson = gsonBuilder.create();
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://"+severAddress).addConverterFactory(GsonConverterFactory.create(gson)).build();
        return  retrofit;
    }
    private class Adapter implements JsonDeserializer<DeviceResource>{
        @Override
        public DeviceResource deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            String packageName ="brelak.dawid.smarthomepilot.model.device.resources.";
            JsonObject jsonObject = json.getAsJsonObject();
            String type = jsonObject.get("type").getAsString();
           // Log.d("[Deserialize]","Response = "+json.toString());
            try {
                return context.deserialize(json,Class.forName(packageName+type));
            } catch (ClassNotFoundException e) {
                Log.d("[Deserialize]","Unknown element type");
            }
            return null;
        }
    }
}
