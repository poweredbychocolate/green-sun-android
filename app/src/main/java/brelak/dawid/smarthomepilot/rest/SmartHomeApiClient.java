package brelak.dawid.smarthomepilot.rest;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import brelak.dawid.smarthomepilot.model.device.basic.DeviceResource;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SmartHomeApiClient {
    public Retrofit getApiClient(String severAddress){
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(DeviceResource.class, new Adapter());
        Gson gson = gsonBuilder.create();
        ////
        OkHttpClient okHttpClient = getUnsafeOkHttpClient();
        ////
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://"+severAddress).client(okHttpClient).addConverterFactory(GsonConverterFactory.create(gson)).build();
        return  retrofit;
    }
    private class Adapter implements JsonDeserializer<DeviceResource>{
        @Override
        public DeviceResource deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            String packageName ="brelak.dawid.smarthomepilot.model.device.resources.";
            JsonObject jsonObject = json.getAsJsonObject();
            String type = jsonObject.get("type").getAsString();
           // Log.d("[Deserialize]","Response = "+json.toString());
            try {
                return context.deserialize(json,Class.forName(packageName+type));
            } catch (ClassNotFoundException e) {
                Log.d("[Deserialize]","Unknown element type");
            }
            return null;
        }
    }
        public  OkHttpClient getUnsafeOkHttpClient() {
            try {
                // Create a trust manager that does not validate certificate chains
                final TrustManager[] trustAllCerts = new TrustManager[] {
                        new X509TrustManager() {
                            @Override
                            public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                            }
                            @Override
                            public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType){
                            }
                            @Override
                            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                return new java.security.cert.X509Certificate[]{};
                            }
                        }
                };
                // Install the all-trusting trust manager
                final SSLContext sslContext = SSLContext.getInstance("SSL");
                sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

                // Create an ssl socket factory with our all-trusting manager
                final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

                OkHttpClient.Builder builder = new OkHttpClient.Builder();
                builder.sslSocketFactory(sslSocketFactory, (X509TrustManager)trustAllCerts[0]);
                builder.hostnameVerifier((hostname, session) -> true);
                OkHttpClient okHttpClient = builder.build();
                return okHttpClient;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
    }
}
