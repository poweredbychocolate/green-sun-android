package brelak.dawid.smarthomepilot.rest;

import java.util.List;

import brelak.dawid.smarthomepilot.model.device.basic.DeviceResource;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface SmartHomeApi {
@GET("api/online/resources/")
    Call<List<DeviceResource>> resourcesList();
    @GET("api/online/resources/{readOnly}")
    Call<List<DeviceResource>> resourcesList(@Path("readOnly") Boolean readOnly);
    @GET("api/online/resource/{id}/{value}")
    Call<DeviceResource> writeDevice(@Path("id") Integer id, @Path("value") Object value);
}
