package brelak.dawid.smarthomepilot.activities;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import brelak.dawid.smarthomepilot.R;
import brelak.dawid.smarthomepilot.dao.DevicesDAO;
import brelak.dawid.smarthomepilot.dao.ServerConnectionDAO;
import brelak.dawid.smarthomepilot.database.LocalDatabase;
import brelak.dawid.smarthomepilot.fragments.DevicesControlFragment;
import brelak.dawid.smarthomepilot.fragments.DevicesFragment;
import brelak.dawid.smarthomepilot.fragments.ServersFragment;
import brelak.dawid.smarthomepilot.model.sqllite.ServerConnection;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, Serializable {

    private DevicesFragment devicesFragment;
    private DevicesControlFragment devicesControlFragment;
    private ServersFragment serversFragment;
    private DevicesDAO devicesDAO;
    private ServerConnection serverConnection;
    private NavigationView navigationView;
    private ProgressBar progressBar;
    private String lastFragmentName;
    private AtomicInteger progressCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressCount = new AtomicInteger(0);
        // read default connection
        ActiveConnThread activeConnThread = new ActiveConnThread(LocalDatabase.getLocalDatabase(this).serverConnectionDAO());
        activeConnThread.start();

        Toolbar topToolbar = findViewById(R.id.top_toolbar);
        setSupportActionBar(topToolbar);

        DrawerLayout smarthomeLayout = findViewById(R.id.smarthome_layout);
        ActionBarDrawerToggle draverBarToggle = new ActionBarDrawerToggle(
                this, smarthomeLayout, topToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        smarthomeLayout.addDrawerListener(draverBarToggle);
        draverBarToggle.syncState();

        navigationView = findViewById(R.id.smarthome_menu);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.menu_devices);
        progressBar = findViewById(R.id.main_progress_bar);
        //wait if default connection will be available
        while (!activeConnThread.finish) {
        }
        serverConnection = activeConnThread.serverConnection;
        devicesDAO = new DevicesDAO(serverConnection);
        //Load default fragment
        devicesFragment = new DevicesFragment();
        devicesControlFragment = new DevicesControlFragment();
        serversFragment = new ServersFragment();
        if (savedInstanceState != null && savedInstanceState.getString("LAST_FRAGMENT") != null) {
            showFragmentByName(savedInstanceState.getString("LAST_FRAGMENT"), navigationView);
        } else {
            showFragment(devicesFragment, navigationView);
        }

    }

    /*
    close left menu if open
     */
    @Override
    public void onBackPressed() {

        DrawerLayout drawer = findViewById(R.id.smarthome_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void showFragment(Fragment fragment, NavigationView navigationView) {
        hideFragments();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (fragment.isAdded())
            fragmentTransaction.show(fragment);
        else
            fragmentTransaction.add(R.id.main_fragment, fragment);
        lastFragmentName = fragment.getClass().getSimpleName();
        navigationView.setCheckedItem(fragment.getId());
        fragmentTransaction.commit();
    }

    private void hideFragments() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        ////Hide all fragment
        if (devicesControlFragment.isAdded())
            fragmentTransaction.hide(devicesControlFragment);
        if (devicesFragment.isAdded())
            fragmentTransaction.hide(devicesFragment);
        if (serversFragment.isAdded())
            fragmentTransaction.hide(serversFragment);
        fragmentTransaction.commit();
    }

    private void showFragmentByName(String fragmanName, NavigationView navigationView) {
        Fragment fragment = null;
        if (devicesFragment.getClass().getSimpleName().equals(fragmanName)) {
            fragment = devicesFragment;
        } else if (devicesControlFragment.getClass().getSimpleName().equals(fragmanName)) {
            fragment = devicesControlFragment;
        } else if (serversFragment.getClass().getSimpleName().equals(fragmanName)) {
            fragment = serversFragment;
        }
        if (fragment != null) {
            showFragment(fragment, navigationView);
        }
    }

    @Override
    /*
        Action on left menu item select
     */
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        // Handle navigation view item clicks here.

        int id = menuItem.getItemId();
        if (id == R.id.menu_devices) {
            showFragment(devicesFragment, navigationView);
            devicesFragment.reload();
        } else if (id == R.id.menu_devices_control) {
            showFragment(devicesControlFragment, navigationView);
            devicesControlFragment.reload();

        } else if (id == R.id.menu_servers_list) {
            showFragment(serversFragment, navigationView);
        }

        DrawerLayout drawer = findViewById(R.id.smarthome_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("LAST_FRAGMENT", lastFragmentName);
    }

    public DevicesDAO getDevicesDAO() {
        return devicesDAO;
    }

    public void showProgressBar() {
        progressCount.incrementAndGet();
        this.progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        if (progressCount.decrementAndGet() <= 0) {
            this.progressBar.setVisibility(View.GONE);
            progressCount.set(0);
        }
    }

    public void setServerConnection(ServerConnection serverConnection) {
        this.serverConnection = serverConnection;
        this.devicesDAO.changeServerConnection(serverConnection);
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideFragments();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showFragmentByName(lastFragmentName, navigationView);
    }

    ////
    private class ActiveConnThread extends Thread {
        private boolean finish = false;
        private ServerConnection serverConnection;
        private ServerConnectionDAO dao;

        public ActiveConnThread(ServerConnectionDAO dao) {
            this.dao = dao;
        }

        @Override
        public void run() {
            List<ServerConnection> list = dao.getByActiveField(true);
            if (list != null && list.size() > 0) {
                //Load active connection selected by user
                serverConnection = list.get(0);
            } else {
                list = dao.getAll();
                if (list != null && list.size() > 0) {
                    //if active not found , load first connections from all available
                    serverConnection = list.get(0);
                } else {
                    //return default local connection
                    serverConnection = new ServerConnection();
                    serverConnection.setActive(true);
                    serverConnection.setName("Local");
                    serverConnection.setAddress("10.0.0.2");
                }
            }
            finish = true;
        }
    }
}
