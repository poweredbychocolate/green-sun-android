package brelak.dawid.smarthomepilot.adapters;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.List;

import brelak.dawid.smarthomepilot.R;
import brelak.dawid.smarthomepilot.activities.MainActivity;
import brelak.dawid.smarthomepilot.dao.ServerConnectionDAO;
import brelak.dawid.smarthomepilot.database.LocalDatabase;
import brelak.dawid.smarthomepilot.model.sqllite.ServerConnection;

public class ServersViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ServerConnectionDAO dao;
    private List<ServerConnection> serverConnections;
    private MainActivity mainActivity;

    public ServersViewAdapter(LocalDatabase localDatabase, MainActivity mainActivity) {
        this.dao = localDatabase.serverConnectionDAO();
        this.mainActivity=mainActivity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_server, viewGroup, false);
        return new ServersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int index) {
        ServerConnection serverConnection = serverConnections.get(index);
        ((ServersViewHolder) viewHolder).nameView.setText(serverConnection.getName());
        ((ServersViewHolder) viewHolder).addressView.setText(serverConnection.getAddress());
        if (!serverConnection.getActive()) {
            if ((index & 1) == 0) {
                ((ServersViewHolder) viewHolder).layout.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorServers1));
            } else {
                ((ServersViewHolder) viewHolder).layout.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorServers2));
            }
        } else {
            ((ServersViewHolder) viewHolder).layout.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorServersActive));
        }
        View view = ((ServersViewHolder) viewHolder).view;

        view.setOnClickListener((v) -> {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mainActivity);
            dialogBuilder.setTitle(mainActivity.getResources().getString(R.string.dialog_conn_title));
            dialogBuilder.setMessage(mainActivity.getResources().getString(R.string.dialog_conn_msg));
            dialogBuilder.setPositiveButton(mainActivity.getResources().getString(R.string.dialog_conn_yes), (dialog, which) -> {
                ActiveConnection activeConnection = new ActiveConnection(serverConnection);
                activeConnection.start();
                while (!activeConnection.finish) {
                }
                mainActivity.setServerConnection(serverConnection);
                notifyDataSetChanged();
            });
            dialogBuilder.setNegativeButton(mainActivity.getResources().getString(R.string.dialog_cancel),null);
            dialogBuilder.create().show();

        });
        view.setOnLongClickListener((v) -> {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mainActivity);
            dialogBuilder.setTitle(mainActivity.getResources().getString(R.string.dialog_delete_title));
            dialogBuilder.setMessage(mainActivity.getResources().getString(R.string.dialog_delete_msg));
            dialogBuilder.setPositiveButton(mainActivity.getResources().getString(R.string.dialog_delete_yes), (dialog, which) -> {
                DeleteItem deleteItem = new DeleteItem(serverConnection);
                deleteItem.start();
                while (!deleteItem.finish) {
                }
                this.notifyDataSetChanged();
            });
            dialogBuilder.setNegativeButton(mainActivity.getResources().getString(R.string.dialog_cancel),null);
            dialogBuilder.create().show();
            return true;
        });
    }

    @Override
    public int getItemCount() {

        ListLoader thread = new ListLoader();
        thread.start();
        while (!thread.finish) {

        }
        serverConnections = thread.list;
        return serverConnections.size();
    }

    private class ServersViewHolder extends RecyclerView.ViewHolder {
        private TextView nameView;
        private TextView addressView;
        private RelativeLayout layout;
        private View view;

        ServersViewHolder(View view) {
            super(view);
            this.view = view;
            nameView = view.findViewById(R.id.list_servers_name);
            addressView = view.findViewById(R.id.list_servers_address);
            layout = view.findViewById(R.id.list_servers_layout);
        }

    }

    private class ListLoader extends Thread {
        private boolean finish = false;
        private List<ServerConnection> list;

        @Override
        public void run() {
            list = dao.getAll();
            this.finish = true;
        }
    }

    private class DeleteItem extends Thread {
        private boolean finish = false;
        private ServerConnection connection;

        DeleteItem(ServerConnection serverConnection) {
            this.connection = serverConnection;
        }

        @Override
        public void run() {
            dao.delete(connection);
            this.finish = true;
        }
    }

    private class ActiveConnection extends Thread {
        private boolean finish = false;
        private ServerConnection connection;

        ActiveConnection(ServerConnection serverConnection) {
            this.connection = serverConnection;
        }

        @Override
        public void run() {
            List<ServerConnection> list = dao.getByActiveField(true);
            if (list != null && list.size() > 0) {
                ServerConnection tmp = list.get(0);
                tmp.setActive(false);
                dao.update(tmp);
            }
            connection.setActive(true);
            dao.update(connection);
            this.finish = true;
        }
    }
}
