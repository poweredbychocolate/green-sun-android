package brelak.dawid.smarthomepilot.adapters;

import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import brelak.dawid.smarthomepilot.R;
import brelak.dawid.smarthomepilot.activities.MainActivity;
import brelak.dawid.smarthomepilot.dao.DevicesDAO;
import brelak.dawid.smarthomepilot.model.device.basic.DeviceResource;
import brelak.dawid.smarthomepilot.model.device.resources.BooleanResource;
import brelak.dawid.smarthomepilot.model.device.resources.CharacterResource;
import brelak.dawid.smarthomepilot.model.device.resources.FloatResource;
import brelak.dawid.smarthomepilot.model.device.resources.IntegerResource;

public class DevicesControlViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private DevicesDAO devicesDAO;
    private List<DeviceResource> deviceResources;
    private Resources resources;
    private MainActivity mainActivity;
    ////
    public static final int BOOLEAN = 0;
    public static final int CHARACTER = 1;
    public static final int INTEGER = 2;
    public static final int FLOAT = 3;

    public DevicesControlViewAdapter(DevicesDAO devicesDAO, MainActivity mainActivity) {
        this.resources = mainActivity.getResources();
        this.mainActivity = mainActivity;
        this.devicesDAO = devicesDAO;
        this.deviceResources = new ArrayList<>();
        new ListAsyncReader().execute();
    }

    public void refreshAdapter(DevicesDAO devicesDAO) {
        this.devicesDAO = devicesDAO;
        new ListAsyncReader().execute();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view;
        if (viewType == BOOLEAN) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_device_boolean, viewGroup, false);
            return new BooleanHolder(view);
        } else if (viewType == CHARACTER) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_device_character, viewGroup, false);
            return new CharacterHolder(view);
        } else if (viewType == INTEGER) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_device_integer, viewGroup, false);
            return new IntegerHolder(view);
        } else if (viewType == FLOAT) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_device_float, viewGroup, false);
            return new FloatHolder(view);
        }
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_device, viewGroup, false);
        return new DeviceHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int index) {
        if (viewHolder.getItemViewType() == BOOLEAN) {
            BooleanResource booleanResource = (BooleanResource) deviceResources.get(viewHolder.getAdapterPosition());
            BooleanHolder booleanHolder = (BooleanHolder) viewHolder;
            booleanHolder.nameView.setText(booleanResource.getName());
            booleanHolder.stateView.setText(bulidState(booleanResource));
            if (booleanResource.getCurrentState() == null) {
                booleanHolder.toggleButton.setChecked(false);
                booleanHolder.toggleButton.setEnabled(false);
                booleanHolder.strip.setBackgroundColor(resources.getColor(R.color.deviceEmpty));
            } else {
                booleanHolder.toggleButton.setEnabled(true);

                if (booleanResource.getCurrentState().equals(booleanResource.getOnLabel())) {
                    booleanHolder.toggleButton.setChecked(true);
                    booleanHolder.strip.setBackgroundColor(resources.getColor(R.color.deviceBooleanOn));
                } else if (booleanResource.getCurrentState().equals(booleanResource.getOffLabel())) {
                    booleanHolder.toggleButton.setChecked(false);
                    booleanHolder.strip.setBackgroundColor(resources.getColor(R.color.deviceBooleanOff));
                }
            }
            class BooleanAsyncTask extends AsyncTask<Void, Void, BooleanResource> {
                private Boolean asyncIsChecked;

                public BooleanAsyncTask(Boolean asyncIsChecked) {
                    this.asyncIsChecked = asyncIsChecked;
                }

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    mainActivity.showProgressBar();
                    booleanHolder.toggleButton.setEnabled(false);
                }

                @Override
                protected BooleanResource doInBackground(Void... voids) {
                    BooleanResource localResource;
                    if (asyncIsChecked) {
                        localResource = devicesDAO.writeDevice(booleanResource, booleanResource.getOnLabel());
                    } else {
                        localResource = devicesDAO.writeDevice(booleanResource, booleanResource.getOffLabel());
                    }
                    Log.d("[BooleanAsyncTask]", localResource.toString());
                    return localResource;
                }

                @Override
                protected void onPostExecute(BooleanResource postResource) {
                    super.onPostExecute(postResource);
                    deviceResources.set(index, postResource);
                    //asyncBooleanHolder.switchView.setEnabled(true);
                   // notifyItemChanged(viewHolder.getAdapterPosition());
                    notifyDataSetChanged();
                    Timer timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            mainActivity.runOnUiThread(()->new ListAsyncReader().execute());
                        }
                    }, 2000);
                    mainActivity.hideProgressBar();
                }
            }
            booleanHolder.toggleButton.setOnClickListener(v -> {
                boolean checked = ((ToggleButton) v).getText().equals(((ToggleButton) v).getTextOn()) ? true : false;
                new BooleanAsyncTask(checked).execute();
            });
        }
        ////
        else if (viewHolder.getItemViewType() == CHARACTER)

        {
            CharacterResource characterResource = (CharacterResource) deviceResources.get(viewHolder.getAdapterPosition());
            CharacterHolder characterHolder = (CharacterHolder) viewHolder;
            characterHolder.nameView.setText(characterResource.getName());
            characterHolder.stateView.setText(bulidState(characterResource));
            InputFilter filter = new InputFilter.LengthFilter(characterResource.getMaxLength());
            characterHolder.editTextView.setFilters(new InputFilter[]{filter});
            if (characterResource.getCurrentState() == null) {
                characterHolder.editTextView.setEnabled(false);
                characterHolder.strip.setBackgroundColor(resources.getColor(R.color.deviceEmpty));
            } else {
                characterHolder.editTextView.setEnabled(true);
                characterHolder.strip.setBackgroundColor(resources.getColor(R.color.deviceCharacter));
            }
            class CharacterAsyncTask extends AsyncTask<Void, Void, CharacterResource> {
                private CharSequence s;

                public CharacterAsyncTask(CharSequence s) {
                    this.s = s;
                }

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    characterHolder.editTextView.setEnabled(false);
                    mainActivity.showProgressBar();
                }

                @Override
                protected CharacterResource doInBackground(Void... voids) {
                    if (!TextUtils.isEmpty(s.toString())) {
                        CharacterResource localResource = devicesDAO.writeDevice(characterResource, String.valueOf(s));
                        // Log.d("[AsyncResponse]","Character Edit text : "+localResource.toString() );
                        return localResource;
                    }
                    return characterResource;
                }

                @Override
                protected void onPostExecute(CharacterResource postResource) {
                    super.onPostExecute(postResource);
                    characterHolder.editTextView.getText().clear();
                    deviceResources.set(index, postResource);
                    //notifyItemChanged(viewHolder.getAdapterPosition());
                    notifyDataSetChanged();
                    Timer timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            mainActivity.runOnUiThread(()->new ListAsyncReader().execute());
                        }
                    }, 2000);
                    mainActivity.hideProgressBar();
                }
            }
            characterHolder.commitButton.setOnClickListener(v -> {
                new CharacterAsyncTask(characterHolder.editTextView.getText()).execute();
            });
            ////
        } else if (viewHolder.getItemViewType() == INTEGER) {
            IntegerResource integerResource = (IntegerResource) deviceResources.get(viewHolder.getAdapterPosition());
            IntegerHolder integerHolder = (IntegerHolder) viewHolder;
            integerHolder.nameView.setText(integerResource.getName());
            integerHolder.stateView.setText(bulidState(integerResource));
            Integer seekBarOffset = -integerResource.getMinValue();
            integerHolder.seekBarView.setMax(integerResource.getMaxValue() + seekBarOffset);

            if (integerResource.getCurrentState() == null) {
                integerHolder.seekBarView.setEnabled(false);
                integerHolder.strip.setBackgroundColor(resources.getColor(R.color.deviceEmpty));
            } else {
                integerHolder.seekBarView.setEnabled(true);
                integerHolder.seekBarView.setProgress((Integer) integerResource.getCurrentState() + seekBarOffset);
                integerHolder.strip.setBackgroundColor(resources.getColor(R.color.deviceInteger));
            }

            class IntegerAsyncTask extends AsyncTask<Void, Void, IntegerResource> {
                private Integer seekBarValue;

                public IntegerAsyncTask(Integer value) {
                    this.seekBarValue = value;
                }

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    mainActivity.showProgressBar();
                    integerHolder.seekBarView.setEnabled(false);
                }

                @Override
                protected IntegerResource doInBackground(Void... voids) {
                    IntegerResource localResource = devicesDAO.writeDevice(integerResource, seekBarValue);
                    // Log.d("[AsyncResponse]","Integer Seekbar : "+localResource.toString() );
                    return localResource;
                }

                @Override
                protected void onPostExecute(IntegerResource postResource) {
                    super.onPostExecute(postResource);
                    deviceResources.set(index, postResource);
                    //notifyItemChanged(viewHolder.getAdapterPosition());
                    notifyDataSetChanged();
                    Timer timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            mainActivity.runOnUiThread(()->new ListAsyncReader().execute());
                        }
                    }, 2000);

                    mainActivity.hideProgressBar();
                }
            }

            integerHolder.seekBarView.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    integerHolder.stateView.setText(Integer.toString(progress - seekBarOffset));
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    // Log.d("[Item click ]","Integer Seekbar");
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    Integer value = seekBar.getProgress() - seekBarOffset;
                    new IntegerAsyncTask(value).execute();

                }
            });
            ////
        } else if (viewHolder.getItemViewType() == FLOAT)

        {
            FloatResource floatResource = (FloatResource) deviceResources.get(viewHolder.getAdapterPosition());
            FloatHolder floatHolder = (FloatHolder) viewHolder;
            floatHolder.nameView.setText(floatResource.getName());
            floatHolder.stateView.setText(bulidState(floatResource));
            Integer seekBarOffset = -floatResource.getMinValue().intValue();
            floatHolder.bigSeekBarView.setMax(floatResource.getMaxValue().intValue() + seekBarOffset);

            if (floatResource.getCurrentState() == null) {
                floatHolder.bigSeekBarView.setEnabled(false);
                floatHolder.smallSeekBarView.setEnabled(false);
                floatHolder.strip.setBackgroundColor(resources.getColor(R.color.deviceEmpty));
            } else {
                floatHolder.bigSeekBarView.setEnabled(true);
                floatHolder.smallSeekBarView.setEnabled(true);
                floatHolder.strip.setBackgroundColor(resources.getColor(R.color.deviceFloat));
                Float progressValue = (Float) floatResource.getCurrentState();
                floatHolder.bigSeekBarView.setProgress(progressValue.intValue() + seekBarOffset);
                floatHolder.smallSeekBarView.setProgress((int) ((progressValue - progressValue.intValue()) * 100));
            }
            class FloatAsyncTask extends AsyncTask<Void, Void, FloatResource> {
                private Float value;

                public FloatAsyncTask(Float value) {
                    this.value = value;
                }

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    mainActivity.showProgressBar();
                }

                @Override
                protected FloatResource doInBackground(Void... voids) {
                    FloatResource localResource = devicesDAO.writeDevice(floatResource, value);
                    //  Log.d("[AsyncResponse]","Float SeekBar : "+localResource.toString() );
                    return localResource;
                }

                @Override
                protected void onPostExecute(FloatResource postResource) {
                    super.onPostExecute(postResource);
                    deviceResources.set(index, postResource);
                    //notifyItemChanged(viewHolder.getAdapterPosition());
                    notifyDataSetChanged();
                    Timer timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            mainActivity.runOnUiThread(()->new ListAsyncReader().execute());
                        }
                    }, 2000);
                    mainActivity.hideProgressBar();
                }
            }

            class FloatListener implements SeekBar.OnSeekBarChangeListener {
                private Timer timer = new Timer();
                private Float value = (Float) floatResource.getCurrentState();

                public FloatListener() {
                }

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if(value==null)
                        value=0f;
                    if (seekBar.equals(floatHolder.bigSeekBarView)) {
                        float tmp = value - value.intValue();
                        value = seekBar.getProgress() + tmp;
                    }
                    if (seekBar.equals(floatHolder.smallSeekBarView)) {
                        int tmp = value.intValue();
                        value = tmp + (seekBar.getProgress() * 0.01f);
                    }
                    value = value - seekBarOffset;
                    if (value >= floatResource.getMaxValue())
                        value = floatResource.getMaxValue();
                    floatHolder.stateView.setText(Float.toString(value));
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    timer.cancel();
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    if (seekBar.equals(floatHolder.bigSeekBarView)) {
                        float tmp = value - value.intValue();
                        value = seekBar.getProgress() + tmp;
                    }
                    if (seekBar.equals(floatHolder.smallSeekBarView)) {
                        int tmp = value.intValue();
                        value = tmp + (seekBar.getProgress() * 0.01f);
                    }
                    value = value - seekBarOffset;
                    if (value >= floatResource.getMaxValue())
                        value = floatResource.getMaxValue();
                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            mainActivity.runOnUiThread(() -> new FloatAsyncTask(value).execute()
                            );
                        }
                    }, 2000);
                }
            }

            FloatListener floatListener = new FloatListener();
            floatHolder.bigSeekBarView.setOnSeekBarChangeListener(floatListener);
            floatHolder.smallSeekBarView.setOnSeekBarChangeListener(floatListener);
        } else {
            DeviceResource deviceResource = deviceResources.get(index);
            DeviceHolder deviceHolder = (DeviceHolder) viewHolder;
            if (deviceResource == null) {
                deviceHolder.nameView.setText("Removed device");
                deviceHolder.stateView.setText(resources.getString(R.string.info_not_available));
                deviceHolder.strip.setBackgroundColor(resources.getColor(R.color.deviceEmpty));
            } else {
                deviceHolder.nameView.setText(deviceResource.getName());
                String state = deviceResource.getCurrentState() != null ? deviceResource.getCurrentState().toString() : resources.getString(R.string.info_not_available);
                deviceHolder.stateView.setText(state);
                if (deviceResource.getCurrentState() == null) {
                    deviceHolder.strip.setBackgroundColor(resources.getColor(R.color.deviceEmpty));
                } else {
                    deviceHolder.strip.setBackgroundColor(resources.getColor(R.color.white));
                }
            }
        }
    }

    private String bulidState(DeviceResource deviceResource) {
        return deviceResource.getCurrentState() != null ? deviceResource.getCurrentState().toString() : resources.getString(R.string.info_not_available);
    }

    @Override
    public int getItemCount() {
        return deviceResources!=null?deviceResources.size():0;
    }

    @Override
    public int getItemViewType(int position) {
        if (deviceResources.get(position) instanceof BooleanResource) return BOOLEAN;
        if (deviceResources.get(position) instanceof CharacterResource) return CHARACTER;
        if (deviceResources.get(position) instanceof IntegerResource) return INTEGER;
        if (deviceResources.get(position) instanceof FloatResource) return FLOAT;
        return -1;
    }

    class DeviceHolder extends RecyclerView.ViewHolder {
        private TextView nameView;
        private TextView stateView;
        private RelativeLayout layout;
        private View view;
        private View strip;

        public DeviceHolder(@NonNull View view) {
            super(view);
            this.view = view;
            nameView = view.findViewById(R.id.device_name);
            stateView = view.findViewById(R.id.device_state);
            layout = view.findViewById(R.id.device_item);
            strip = view.findViewById(R.id.device_strip);
        }
    }

    class BooleanHolder extends RecyclerView.ViewHolder {
        private TextView nameView;
        private TextView stateView;
        private RelativeLayout layout;
        private ToggleButton toggleButton;
        private View view;
        private View strip;

        public BooleanHolder(@NonNull View view) {
            super(view);
            this.view = view;
            nameView = view.findViewById(R.id.boolean_name);
            stateView = view.findViewById(R.id.boolean_state);
            toggleButton = view.findViewById(R.id.boolean_switch);
            layout = view.findViewById(R.id.boolean_item);
            strip = view.findViewById(R.id.boolean_strip);
        }
    }

    class CharacterHolder extends RecyclerView.ViewHolder {
        private TextView nameView;
        private TextView stateView;
        private RelativeLayout layout;
        private EditText editTextView;
        private Button commitButton;
        private View view;
        private View strip;

        public CharacterHolder(@NonNull View view) {
            super(view);
            this.view = view;
            nameView = view.findViewById(R.id.character_name);
            stateView = view.findViewById(R.id.character_state);
            editTextView = view.findViewById(R.id.character_edit);
            layout = view.findViewById(R.id.character_item);
            commitButton =view.findViewById(R.id.character_commit);
            strip = view.findViewById(R.id.character_strip);
        }
    }

    class IntegerHolder extends RecyclerView.ViewHolder {
        private TextView nameView;
        private TextView stateView;
        private RelativeLayout layout;
        private SeekBar seekBarView;
        private View view;
        private View strip;

        public IntegerHolder(@NonNull View view) {
            super(view);
            this.view = view;
            nameView = view.findViewById(R.id.integer_name);
            stateView = view.findViewById(R.id.integer_state);
            seekBarView = view.findViewById(R.id.integer_seek_bar);
            layout = view.findViewById(R.id.integer_item);
            strip = view.findViewById(R.id.integer_strip);
        }
    }

    class FloatHolder extends RecyclerView.ViewHolder {
        private TextView nameView;
        private TextView stateView;
        private RelativeLayout layout;
        private SeekBar bigSeekBarView;
        private SeekBar smallSeekBarView;
        private View view;
        private View strip;

        public FloatHolder(@NonNull View view) {
            super(view);
            this.view = view;
            nameView = view.findViewById(R.id.float_name);
            stateView = view.findViewById(R.id.float_state);
            bigSeekBarView = view.findViewById(R.id.float_seek_bar_big);
            smallSeekBarView = view.findViewById(R.id.float_seek_bar_small);
            layout = view.findViewById(R.id.float_item);
            strip = view.findViewById(R.id.float_strip);
        }
    }

    class ListAsyncReader extends AsyncTask<Void, Void, ArrayList<DeviceResource>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mainActivity.showProgressBar();
        }

        @Override
        protected ArrayList<DeviceResource> doInBackground(Void... voids) {
            ArrayList<DeviceResource> list = (ArrayList<DeviceResource>) devicesDAO.getDevicesList(false);
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<DeviceResource> list) {
            super.onPostExecute(list);
            deviceResources = list;
            notifyDataSetChanged();
            mainActivity.hideProgressBar();
        }
    }
}
