package brelak.dawid.smarthomepilot.adapters;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import brelak.dawid.smarthomepilot.R;
import brelak.dawid.smarthomepilot.activities.MainActivity;
import brelak.dawid.smarthomepilot.dao.DevicesDAO;
import brelak.dawid.smarthomepilot.model.device.basic.DeviceResource;
import brelak.dawid.smarthomepilot.model.device.resources.BooleanResource;
import brelak.dawid.smarthomepilot.model.device.resources.CharacterResource;
import brelak.dawid.smarthomepilot.model.device.resources.FloatResource;
import brelak.dawid.smarthomepilot.model.device.resources.IntegerResource;

public class DevicesViewAdapter extends RecyclerView.Adapter<DevicesViewAdapter.DeviceHolder> {

    private DevicesDAO devicesDAO;
    private List<DeviceResource> deviceResources;
    private MainActivity mainActivity;
    private Resources resources;

    public DevicesViewAdapter(DevicesDAO devicesDAO, MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        this.resources = mainActivity.getResources();
        this.devicesDAO = devicesDAO;
        this.deviceResources = new ArrayList<>();
        new ListAsyncReader().execute();
    }

    public void refreshAdapter(DevicesDAO devicesDAO) {
        this.devicesDAO = devicesDAO;
        new ListAsyncReader().execute();
    }

    @NonNull
    @Override
    public DeviceHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_device, viewGroup, false);
        return new DeviceHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DeviceHolder deviceHolder, int index) {
        DeviceResource deviceResource = deviceResources.get(index);
        deviceHolder.nameView.setText(deviceResource.getName());
        String state = deviceResource.getCurrentState() != null ? deviceResource.getCurrentState().toString() : resources.getString(R.string.info_not_available);
        deviceHolder.stateView.setText(state);
        if (deviceResource.getCurrentState() == null) {
            deviceHolder.strip.setBackgroundColor(resources.getColor(R.color.deviceEmpty));
        } else {
            if (deviceResource instanceof IntegerResource) {
                deviceHolder.strip.setBackgroundColor(resources.getColor(R.color.deviceInteger));
            } else if (deviceResource instanceof FloatResource) {
                deviceHolder.strip.setBackgroundColor(resources.getColor(R.color.deviceFloat));
            } else if (deviceResource instanceof CharacterResource) {
                deviceHolder.strip.setBackgroundColor(resources.getColor(R.color.deviceCharacter));
            } else if (deviceResource instanceof BooleanResource && deviceResource.getCurrentState().equals(((BooleanResource) deviceResource).getOnLabel())) {
                deviceHolder.strip.setBackgroundColor(resources.getColor(R.color.deviceBooleanOn));
            } else if (deviceResource instanceof BooleanResource && deviceResource.getCurrentState().equals(((BooleanResource) deviceResource).getOffLabel())) {
                deviceHolder.strip.setBackgroundColor(resources.getColor(R.color.deviceBooleanOff));
            }
        }
    }

    @Override
    public int getItemCount() {
        return deviceResources!=null?deviceResources.size():0;
    }

    class DeviceHolder extends RecyclerView.ViewHolder {
        private TextView nameView;
        private TextView stateView;
        private RelativeLayout layout;
        private View view;
        private View strip;

        public DeviceHolder(@NonNull View view) {
            super(view);
            this.view = view;
            nameView = view.findViewById(R.id.device_name);
            stateView = view.findViewById(R.id.device_state);
            layout = view.findViewById(R.id.device_item);
            strip = view.findViewById(R.id.device_strip);
        }
    }

    class ListAsyncReader extends AsyncTask<Void, Void, ArrayList<DeviceResource>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mainActivity.showProgressBar();
        }

        @Override
        protected ArrayList<DeviceResource> doInBackground(Void... voids) {
            ArrayList<DeviceResource> list = (ArrayList<DeviceResource>) devicesDAO.getDevicesList();
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<DeviceResource> list) {
            super.onPostExecute(list);
            deviceResources = list;
            notifyDataSetChanged();
            mainActivity.hideProgressBar();
        }
    }
}
