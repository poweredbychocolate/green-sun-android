package brelak.dawid.smarthomepilot.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import brelak.dawid.smarthomepilot.R;
import brelak.dawid.smarthomepilot.activities.MainActivity;
import brelak.dawid.smarthomepilot.adapters.DevicesViewAdapter;


public class DevicesFragment extends Fragment {
    private MainActivity mainActivity;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout deviceSwipeRefresh;

    public DevicesFragment() {
        super();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View devicesView = inflater.inflate(R.layout.fragment_devices, container, false);
        mainActivity = (MainActivity) getActivity();
        deviceSwipeRefresh = devicesView.findViewById(R.id.devices_swipe_refresh);
        recyclerView = devicesView.findViewById(R.id.devices_list_view);
        return devicesView;
    }

    public void reload() {
        if (deviceSwipeRefresh != null && recyclerView != null) {
            deviceSwipeRefresh.setRefreshing(true);
            ((DevicesViewAdapter) recyclerView.getAdapter()).refreshAdapter(mainActivity.getDevicesDAO());
            deviceSwipeRefresh.setRefreshing(false);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        deviceSwipeRefresh.setRefreshing(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new DevicesViewAdapter(mainActivity.getDevicesDAO(), mainActivity));
        deviceSwipeRefresh.setOnRefreshListener(() -> {
            ((DevicesViewAdapter) recyclerView.getAdapter()).refreshAdapter(mainActivity.getDevicesDAO());
            deviceSwipeRefresh.setRefreshing(false);
        });
        deviceSwipeRefresh.setRefreshing(false);
    }
}
