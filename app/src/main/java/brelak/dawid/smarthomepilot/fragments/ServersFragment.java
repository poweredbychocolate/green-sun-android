package brelak.dawid.smarthomepilot.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import brelak.dawid.smarthomepilot.R;
import brelak.dawid.smarthomepilot.activities.MainActivity;
import brelak.dawid.smarthomepilot.adapters.ServersViewAdapter;
import brelak.dawid.smarthomepilot.dao.ServerConnectionDAO;
import brelak.dawid.smarthomepilot.database.LocalDatabase;
import brelak.dawid.smarthomepilot.model.sqllite.ServerConnection;


public class ServersFragment extends Fragment {
    private MainActivity mainActivity;
    private LocalDatabase localDatabase;
    private RecyclerView serversRecyclerView;
    ////
    private EditText nameEditText;
    private EditText addressEditText;
    private Button button;

    public ServersFragment() {
        super();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View devicesView = inflater.inflate(R.layout.fragment_servers, container, false);
        mainActivity = (MainActivity) getActivity();
        localDatabase = LocalDatabase.getLocalDatabase(mainActivity.getApplicationContext());
        serversRecyclerView = devicesView.findViewById(R.id.servers_list_view);
        nameEditText = devicesView.findViewById(R.id.list_new_name);
        addressEditText = devicesView.findViewById(R.id.list_new_address);
        button = devicesView.findViewById(R.id.list_new_button);
        return devicesView;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        serversRecyclerView.setLayoutManager(new LinearLayoutManager(mainActivity.getApplicationContext()));
        serversRecyclerView.setAdapter(new ServersViewAdapter(localDatabase, mainActivity));
        button.setOnClickListener((v) -> {
            if (TextUtils.isEmpty(nameEditText.getText().toString())) {
                nameEditText.setError(getResources().getString(R.string.label_cannot_empty));
                return;
            }
            if (TextUtils.isEmpty(addressEditText.getText().toString())) {
                addressEditText.setError(getResources().getString(R.string.label_cannot_empty));
                return;
            }
            ServerConnection sc = new ServerConnection();
            sc.setActive(false);
            sc.setAddress(addressEditText.getText().toString());
            sc.setName(nameEditText.getText().toString());
            InsertThread thread = new InsertThread(localDatabase.serverConnectionDAO(), sc);
            thread.start();
            nameEditText.getText().clear();
            addressEditText.getText().clear();
            while (!thread.finish) {
            }
            serversRecyclerView.getAdapter().notifyDataSetChanged();
            Toast.makeText(getContext(), getResources().getString(R.string.info_server_added), Toast.LENGTH_LONG).show();
        });
    }

    private class InsertThread  extends Thread {

        private boolean finish = false;
        private ServerConnection connection;
        private ServerConnectionDAO dao;

        InsertThread(ServerConnectionDAO serverConnectionDAO, ServerConnection serverConnection) {
            this.connection = serverConnection;
            this.dao = serverConnectionDAO;
        }

        @Override
        public void run() {
            dao.insert(connection);
            finish = true;
        }

    }
}
