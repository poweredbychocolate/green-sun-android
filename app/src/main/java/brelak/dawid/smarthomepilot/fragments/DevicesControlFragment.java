package brelak.dawid.smarthomepilot.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import brelak.dawid.smarthomepilot.R;
import brelak.dawid.smarthomepilot.activities.MainActivity;
import brelak.dawid.smarthomepilot.adapters.DevicesControlViewAdapter;

public class DevicesControlFragment extends Fragment {
    private MainActivity mainActivity;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout controlSwipeRefresh;

    public DevicesControlFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View devicesView = inflater.inflate(R.layout.fragment_devices_control, container, false);
        mainActivity = (MainActivity) getActivity();
        controlSwipeRefresh = devicesView.findViewById(R.id.control_swipe_refresh);
        recyclerView = devicesView.findViewById(R.id.control_list_view);
        return devicesView;
    }

    public void reload() {
        if (controlSwipeRefresh != null && recyclerView != null) {
            controlSwipeRefresh.setRefreshing(true);
            ((DevicesControlViewAdapter) recyclerView.getAdapter()).refreshAdapter(mainActivity.getDevicesDAO());
            controlSwipeRefresh.setRefreshing(false);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        controlSwipeRefresh.setRefreshing(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new DevicesControlViewAdapter(mainActivity.getDevicesDAO(), mainActivity));
        controlSwipeRefresh.setOnRefreshListener(() -> {
            ((DevicesControlViewAdapter) recyclerView.getAdapter()).refreshAdapter(mainActivity.getDevicesDAO());
            controlSwipeRefresh.setRefreshing(false);
        });
        controlSwipeRefresh.setRefreshing(false);
    }
}
