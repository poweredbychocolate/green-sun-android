package brelak.dawid.smarthomepilot.model.device.basic;
import java.io.Serializable;

/**
 * DeviceResource - Basic entity
 * @author Dawid
 * @since 2018-11-01
 * @version 2.0
 */
public abstract class DeviceResource implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	private String resource;
	private Boolean readOnly = false;
	private Device device;

	/**
	 * 
	 * @return
	 */
	public abstract Object getCurrentState();

	public abstract boolean setCurrentState(Object state); 

	public DeviceResource() {
	}

	/**
	 * 
	 * @param name
	 * @param resource
	 * @param device
	 */
	public DeviceResource(String name, String resource, Device device) {
		this.resource = resource;
		this.name = name;
		this.device = device;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the resource
	 */
	public String getResource() {
		return resource;
	}

	/**
	 * @param resource the resource to set
	 */
	public void setResource(String resource) {
		this.resource = resource;
		if (this.name == null)
			name = resource;
	}

	/**
	 * @return the readOnly
	 */
	public Boolean getReadOnly() {
		return readOnly;
	}

	/**
	 * @param readOnly the readOnly to set
	 */
	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}

	/**
	 * @return the device
	 */
	public Device getDevice() {
		return device;
	}

	/**
	 * @param device the device to set
	 */
	public void setDevice(Device device) {
		this.device = device;
	}

    @Override
    public String toString() {
        return "DeviceResource{" +
                "name='" + name + '\'' +
                ", resource='" + resource + '\'' +
                ", readOnly=" + readOnly +
                ", device=" + device +
                '}';
    }
}
