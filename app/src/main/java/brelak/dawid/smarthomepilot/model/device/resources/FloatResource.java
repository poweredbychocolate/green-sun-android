package brelak.dawid.smarthomepilot.model.device.resources;

import brelak.dawid.smarthomepilot.model.device.basic.Device;
import brelak.dawid.smarthomepilot.model.device.basic.DeviceResource;

/**
 * FloatingPoint {@link DeviceResource} that can be set floating point value for user define range 
 * @author Dawid
 * @since 2018-11-01
 * @version 2.0
 */
public class FloatResource extends DeviceResource {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Float currentState;
	private Float minValue;
	private Float maxValue;

	public FloatResource() {
	}

	/**
	 * @param currentState
	 * @param minValue
	 * @param maxValue
	 */
	public FloatResource(String name, String resource, Device device, Float currentState, Float minValue, Float maxValue) {
		super(name,resource, device);
		this.currentState = currentState;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}

	@Override
	public Object getCurrentState() {
		return currentState;
	}

	@Override
	public boolean setCurrentState(Object state) {
		if (state instanceof Float && (Float) state >= minValue && (Float) state <= maxValue) {
			this.currentState = (Float) state;
			return true;
		} else {
			this.currentState = null;
			return false;
		}
	}

	/**
	 * @return the minValue
	 */
	public Float getMinValue() {
		return minValue;
	}

	/**
	 * @param minValue the minValue to set
	 */
	public void setMinValue(Float minValue) {
		this.minValue = minValue;
	}

	/**
	 * @return the maxValue
	 */
	public Float getMaxValue() {
		return maxValue;
	}

	/**
	 * @param maxValue the maxValue to set
	 */
	public void setMaxValue(Float maxValue) {
		this.maxValue = maxValue;
	}

	@Override
	public String toString() {
		return "FloatResource{" +
				"currentState=" + currentState +
				", minValue=" + minValue +
				", maxValue=" + maxValue +
				"} " + super.toString();
	}
}
