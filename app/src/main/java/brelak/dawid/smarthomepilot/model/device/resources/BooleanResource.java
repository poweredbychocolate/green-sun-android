package brelak.dawid.smarthomepilot.model.device.resources;

import brelak.dawid.smarthomepilot.model.device.basic.Device;
import brelak.dawid.smarthomepilot.model.device.basic.DeviceResource;

/**
 * Boolean {@link DeviceResource} can be set only user defined value resource on and resource off
 * @author Dawid
 * @since 2018-11-01
 * @version 3.0
 */

public class BooleanResource extends DeviceResource {
	
	public static final int TRUE=1;
	public static final int FALSE=0;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String currentState;
	private String onLabel;
	private String offLabel;

	public BooleanResource() {
	}

	/**
	 * @param currentState
	 * @param onLabel
	 * @param offLabel
	 */
	public BooleanResource(String name, String resource, Device device, String currentState, String onLabel, String offLabel) {
		super(name,resource, device);
		this.currentState = currentState;
		this.onLabel = onLabel;
		this.offLabel = offLabel;
	}

	@Override
	public Object getCurrentState() {
		return currentState;
	}

	@Override
	public boolean setCurrentState(Object state) {
		if (state instanceof String && (((String) state).equals(onLabel) || ((String) state).equals(offLabel))) {
			this.currentState = (String) state;
			return true;
		} else {
			this.currentState = null;
			return false;
		}

	}

	/**
	 * @return the onLabel
	 */
	public String getOnLabel() {
		return onLabel;
	}

	/**
	 * @param onValue the onLabel to set
	 */
	public void setOnLabel(String onLabel) {
		this.onLabel = onLabel;
	}

	/**
	 * @return the offLabel
	 */
	public String getOffLabel() {
		return offLabel;
	}

	/**
	 * @param offLabel the offLabel to set
	 */
	public void setOffLabel(String offLabel) {
		this.offLabel = offLabel;
	}

	@Override
	public String toString() {
		return "BooleanResource{" +
				"currentState='" + currentState + '\'' +
				", onLabel='" + onLabel + '\'' +
				", offLabel='" + offLabel + '\'' +
				"} " + super.toString();
	}
}
