package brelak.dawid.smarthomepilot.model.sqllite;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;

import java.io.Serializable;

@Entity
public class ServerConnection implements BaseColumns,Serializable {

    @PrimaryKey(autoGenerate = true)
    private Integer id;
    @NonNull
    private String name;
    @NonNull
    private String address;
    @NonNull
    private Boolean active;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getAddress() {
        return address;
    }

    public void setAddress(@NonNull String address) {
        this.address = address;
    }

    @NonNull
    public Boolean getActive() {
        return active;
    }

    public void setActive(@NonNull Boolean active) {
        this.active = active;
    }
}
