package brelak.dawid.smarthomepilot.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import brelak.dawid.smarthomepilot.model.device.basic.DeviceResource;
import brelak.dawid.smarthomepilot.model.device.resources.BooleanResource;
import brelak.dawid.smarthomepilot.model.device.resources.CharacterResource;
import brelak.dawid.smarthomepilot.model.device.resources.FloatResource;
import brelak.dawid.smarthomepilot.model.device.resources.IntegerResource;
import brelak.dawid.smarthomepilot.model.sqllite.ServerConnection;
import brelak.dawid.smarthomepilot.rest.SmartHomeApi;
import brelak.dawid.smarthomepilot.rest.SmartHomeApiClient;
import retrofit2.Call;
import retrofit2.Response;

public class DevicesDAO {

    private ServerConnection connection;
    private SmartHomeApiClient smartHomeApiClient;
    private List<DeviceResource> list;
    private DeviceResource threadResource;

    public DevicesDAO(ServerConnection serverConnection) {
        this.connection = serverConnection;
        smartHomeApiClient = new SmartHomeApiClient();
    }

    public synchronized void changeServerConnection(ServerConnection serverConnection) {
        this.connection = serverConnection;
    }

    public List<DeviceResource> getDevicesList() {
        int charPos = connection.getAddress().indexOf("/");
        if (charPos == -1) {
            SmartHomeApi homeApi = smartHomeApiClient.getApiClient(connection.getAddress()).create(SmartHomeApi.class);
            Call<List<DeviceResource>> call = homeApi.resourcesList();

            try {
                Response<List<DeviceResource>> listResponse = call.execute();
                list = listResponse.body();
            } catch (IOException e) {
                list = new ArrayList<>();
            }

        } else {
            list = new ArrayList<>();
        }
        return list;
    }

    public List<DeviceResource> getDevicesList(Boolean readOnly) {
        int charPos = connection.getAddress().indexOf("/");
        if (charPos == -1) {
            SmartHomeApi homeApi = smartHomeApiClient.getApiClient(connection.getAddress()).create(SmartHomeApi.class);
            Call<List<DeviceResource>> call = homeApi.resourcesList(readOnly);
            try {
                Response<List<DeviceResource>> listResponse = call.execute();
                list = listResponse.body();
            } catch (IOException e) {
                list = new ArrayList<>();
            }
        } else {
            list = new ArrayList<>();
        }
        return list;
    }

    public synchronized DeviceResource setDevice(DeviceResource deviceResource, Object value) {
        threadResource = deviceResource;
        int charPos = connection.getAddress().indexOf("/");
        if (charPos == -1) {
            SmartHomeApi homeApi = smartHomeApiClient.getApiClient(connection.getAddress()).create(SmartHomeApi.class);
            Call<DeviceResource> call = homeApi.writeDevice(threadResource.getId(), value);
                try {
                    threadResource = call.execute().body();
                } catch (IOException e) {
                    threadResource.setCurrentState(null);
                }
        } else {
            threadResource.setCurrentState(null);
        }
        return threadResource;
    }

    public synchronized BooleanResource writeDevice(BooleanResource deviceResource, String value) {
        BooleanResource booleanResource = (BooleanResource) setDevice(deviceResource, value);
        return booleanResource;
    }

    public synchronized CharacterResource writeDevice(CharacterResource deviceResource, String value) {
        CharacterResource characterResource = (CharacterResource) setDevice(deviceResource, value);
        return characterResource;
    }

    public synchronized IntegerResource writeDevice(IntegerResource deviceResource, Integer value) {
        IntegerResource integerResource = (IntegerResource) setDevice(deviceResource, value);
        return integerResource;
    }

    public synchronized FloatResource writeDevice(FloatResource deviceResource, Float value) {
        FloatResource floatResource = (FloatResource) setDevice(deviceResource, value);
        return floatResource;
    }
}

