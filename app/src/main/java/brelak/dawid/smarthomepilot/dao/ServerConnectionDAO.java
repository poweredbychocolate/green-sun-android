package brelak.dawid.smarthomepilot.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.database.Cursor;

import java.util.List;

import brelak.dawid.smarthomepilot.model.sqllite.ServerConnection;

@Dao
public interface ServerConnectionDAO {
    @Insert
    public void insert(ServerConnection connection);
    @Update
    public void update(ServerConnection connection);
    @Delete
    public void delete(ServerConnection connection);
    @Query("SELECT * FROM SERVERCONNECTION")
    public List<ServerConnection> getAll();
    @Query("SELECT * FROM SERVERCONNECTION WHERE active = :value")
    public List<ServerConnection> getByActiveField( boolean value);
    @Query("SELECT COUNT(*) FROM SERVERCONNECTION")
    public int count();

}
