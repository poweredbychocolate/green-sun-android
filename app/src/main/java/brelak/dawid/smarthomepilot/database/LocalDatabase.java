package brelak.dawid.smarthomepilot.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import brelak.dawid.smarthomepilot.dao.ServerConnectionDAO;
import brelak.dawid.smarthomepilot.model.sqllite.ServerConnection;

@Database(entities = {ServerConnection.class},version = 1,exportSchema = false)
public abstract class LocalDatabase extends RoomDatabase {
    ////
    private static LocalDatabase localDatabase;
    public static synchronized   LocalDatabase getLocalDatabase(Context context){
        if(localDatabase==null){
            localDatabase = Room.databaseBuilder(context.getApplicationContext(),LocalDatabase.class,"SmartHomeDB").build();
        }
        return localDatabase;

    }
    ////
    public abstract ServerConnectionDAO serverConnectionDAO();
}
